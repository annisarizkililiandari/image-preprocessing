# Image Preprocessing

Build a preprocessing application for image data, the preprocessing features consist of anticlockwise rotation, clockwise rotation, horizontal flip, vertical flip, adding light effect, adding dark effect, adding noise effect, adding blur effect. Can be switched to grayscale mode. This application was built in the Python programming language and plotly DASH as GUI for visualization.

![Image Preprocessing](https://gitlab.com/annisarizkililiandari/image-preprocessing/-/raw/main/Image_Preprocessing.jpeg)


